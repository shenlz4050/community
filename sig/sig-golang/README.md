# openEuler Golang SIG

# 组织会议

- 公开的会议时间：TBD

# 成员

- caihaomin
- jing-rui
- genedna

### Maintainer列表

- caihaomin[@caihaomin](https://gitee.com/caihaomin)
- jingrui[@jing-rui](https://gitee.com/jing-rui)
- maquanyi[@genedna](https://gitee.com/genedna)

### Committer列表

- caihaomin[@caihaomin](https://gitee.com/caihaomin)
- jingrui[@jing-rui](https://gitee.com/jing-rui)
- maquanyi[@genedna](https://gitee.com/genedna)


# 联系方式

- [邮件列表](sig-golang@openeuler.org)

# 项目清单

项目名称：golang

repository地址：
  - https://gitee.com/src-openeuler/golang
