# openEuler Wine 兴趣小组（SIG）

Wine （“Wine Is Not an Emulator” 的首字母缩写）是一个能够在多种 POSIX-compliant 操作系统（诸如 Linux，macOS 及 BSD 等）上运行 Windows 应用的兼容层。Wine 不是像虚拟机或者模拟器一样模仿内部的 Windows 逻辑，而是將 Windows API 调用翻译成为动态的 POSIX 调用，免除了性能和其他一些行为的内存占用，让你能够干净地集合 Windows 应用到你的桌面。

为 openEuler 引入 Wine ，从而让 Windows 海量的应用能运行在 openEuler 上。

## Wine SIG 组目标

### 工作目标

 - 在 openEuler 社区中添加对 Wine 的支持
 - 及时响应用户反馈，解决相关问题

### 交付物

- 源码、tar包或兼而有之

### 该 SIG 管理的 repository 及描述

- 仓库: https://gitee.com/src-openeuler/wine

## SIG 基本信息

### Maintainers
- vyloy

### Committers
- vyloy

### 邮件列表
- wine@openeuler.org

### 会议
- 时间: 每个月的第一个周五下午，16:00 - 16:30 +0800 北京时间
