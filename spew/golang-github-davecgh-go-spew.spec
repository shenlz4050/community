%bcond_without check
%global goipath         github.com/davecgh/go-spew
Version:             1.1.1
%global common_description %{expand:
Go-spew implements a deep pretty printer for Go data structures to aid in
debugging. A comprehensive suite of tests with 100% test coverage is
provided to ensure proper functionality.}
%gometa
Name:                golang-github-davecgh-go-spew
Release:             1
Summary:             Deep pretty printer for Go data structures to aid in debug
License:             ISC
URL:                 https://%{goipath}
Source0:             https://%{goipath}/archive/v%{version}.tar.gz
Source1:             glide.lock
Source2:             glide.yaml
BuildRequires:       compiler(go-compiler)
%description
%{common_description}

%package devel
Summary:             %{summary}
BuildArch:           noarch
%description devel
%{common_description}
This package contains library source intended for
building other packages which use import path with
%{goipath} prefix.

%prep
%forgesetup
cp %{SOURCE1} %{SOURCE2} .

%install
%goinstall glide.lock glide.yaml
%if %{with check}

%check
%gochecks
%endif

%files devel -f devel.file-list
%license LICENSE
%doc README.md

%changelog
* Tue Sep 8 2020 shenleizhao <shenleizhao@huawei.com> - 1.1.1-1
- package init
